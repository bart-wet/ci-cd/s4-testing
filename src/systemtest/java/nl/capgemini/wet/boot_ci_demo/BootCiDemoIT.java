package nl.capgemini.wet.boot_ci_demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {
        BootCiDemoApplication.class
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BootCiDemoIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void should_get_hello_world_without_name() {

        final ResponseEntity<String> result = restTemplate.getForEntity(
                "http://localhost:" + port + "/",
                String.class
        );

        assertEquals("Hello world\n", result.getBody());
    }

    @Test
    void should_get_hello_world_with_name() {

        final ResponseEntity<String> result = restTemplate.getForEntity(
                "http://localhost:" + port + "/?name=Wednesday Evening",
                String.class
        );

        assertEquals("Hello Wednesday Evening\n", result.getBody());
    }

}
