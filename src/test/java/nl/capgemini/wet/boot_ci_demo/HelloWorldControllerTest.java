package nl.capgemini.wet.boot_ci_demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class HelloWorldControllerTest {

    @InjectMocks
    private HelloWorldController testable;

    @Mock
    private Model modelMock;

    @Test
    void should_get_hello_world_without_name() {

        final String result = testable.sayHello(modelMock, null);

        assertEquals("hello", result);
        verify(modelMock).addAttribute("name", "world");
    }

    @Test
    void should_get_hello_world_with_name() {

        final String result = testable.sayHello(modelMock, "Wednesday Evening");

        assertEquals("hello", result);
        verify(modelMock).addAttribute("name", "Wednesday Evening");
    }

}
