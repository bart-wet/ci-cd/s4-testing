package nl.capgemini.wet.boot_ci_demo;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloWorldController {

    @GetMapping("/")
    public String sayHello(final Model model, @RequestParam(value = "name", required = false) final String name) {
        if (!StringUtils.hasText(name)) {
            model.addAttribute("name", "world");
            return "hello";
        }
        final String temp = Jsoup.clean(name, Whitelist.none());
        model.addAttribute("name", temp);
        return "hello";
    }

}
