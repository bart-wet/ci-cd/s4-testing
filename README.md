# Exploring CI/CD Session 4: Testing

This session uses code from the previous sessions. It is not mandatory to make code changes, but if you like to the 
required software needed is the same for building your own Spring Boot application using Maven.
You can also use this file or create a file to make changes in and see how branching works. 

## Software requirements

- Gitlab account
- Git
- Git GUI-based application (Optionally)
- Java (Optional for actual code changes)
- IDE (Optional for actual code changes)
- Maven (possibly bundled with IDE) (Optional for actual code changes)

## Knowledge requirements

- Git
- General computer usage
- Java language (Optional for actual code changes)

## Getting started

- Fork the repository
    - Using the Gitlab website:
        - Press "Fork" (top-right)
        - Enter your own target workspace
        - Select visibility as you like
- Clone your own repository

